from rest_framework import serializers
from app.topic.models.topic import Topic
from app.post.models.post import Post
from app.topic.serializers.topic import TopicSerializer


class PostSerializer(serializers.ModelSerializer):
    topics = TopicSerializer(many=True)

    class Meta:
        model = Post
        fields = ['id',
                  'author',
                  'title',
                  'content',
                  'topics',
                  'is_published'
                  ]

    def create(self, validated_data):
        topics_data = validated_data.pop('topics')
        post = Post.objects.create(**validated_data, is_published=False)
        for topic_data in topics_data:
            topic, created = Topic.objects.get_or_create(name=topic_data['name'])
            post.topics.add(topic)
        return post

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.content = validated_data.get('content', instance.content)
        if 'is_published' in validated_data and self.context['request'].user.is_superuser:
            instance.is_published = validated_data.get('is_published', instance.is_published)
        instance.save()
        instance.topics.clear()
        topics_data = validated_data.pop('topics')
        for topic_data in topics_data:
            topic, created = Topic.objects.get_or_create(name=topic_data['name'])
            instance.topics.add(topic)
        return instance
