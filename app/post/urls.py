from rest_framework.routers import DefaultRouter

from app.post.views.post import PostViewSet

router = DefaultRouter()

router.register(r'search', PostViewSet, 'post')

urlpatterns = router.urls
