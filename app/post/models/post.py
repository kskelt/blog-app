from django.db import models
from django.contrib.auth import get_user_model

from app.topic.models.topic import Topic

User = get_user_model()

class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name="post")
    title = models.CharField(max_length=200)
    content = models.TextField()
    topics = models.ManyToManyField(Topic, related_name="articles")
    is_published = models.BooleanField(default=False)
