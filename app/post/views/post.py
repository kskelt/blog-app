from rest_framework import viewsets, permissions

from app.post.models.post import Post
from app.post.serializers.post import PostSerializer
from app.topic.models.topic import Topic


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.filter(is_published=True)
    serializer_class = PostSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        serializer.save()

    def perform_update(self, serializer):
        if 'is_published' in serializer.validated_data and self.request.user.is_superuser:
            serializer.save()
        else:
            topics_data = serializer.validated_data.pop('topics')
            instance = serializer.save()
            instance.topics.clear()
            for topic_data in topics_data:
                topic, created = Topic.objects.get_or_create(name=topic_data['name'])
                instance.topics.add(topic)

    def perform_destroy(self, instance):
        if self.request.user.is_superuser or instance.author == self.request.user:
            instance.delete()
