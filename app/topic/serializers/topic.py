from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied

from app.topic.models.topic import Topic
from django.contrib.auth import get_user_model


User = get_user_model()

class TopicSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=False)
    
    class Meta:
        model = Topic
        fields = ['id', 'name']

    def create(self, validated_data):
        if self.context['request'].user.is_superuser:
            if 'name' in validated_data:
                return Topic.objects.create(name=validated_data['name'])
        else:
            raise PermissionDenied("Permission denied")

    def update(self, instance, validated_data):
        if self.context['request'].user.is_superuser:
            if 'name' in validated_data:
                instance.name = validated_data.get('name', instance.name)
                instance.save()
            return instance
        else:
            raise PermissionDenied("Permission denied")

    def destroy(self, instance):
        if self.context['request'].user.is_superuser:
            instance.delete()
        else:
            raise PermissionDenied("Permission denied")

    def to_representation(self, instance):
        if self.context['request'].method == 'GET':
            return TopicSerializer(instance, context=self.context).data
        else:
            return {'id': instance.id}