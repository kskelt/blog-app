from rest_framework import viewsets, permissions
from app.post.models.post import Topic
from app.post.serializers.post import TopicSerializer

class TopicViewSet(viewsets.ModelViewSet):
    queryset = Topic.objects.filter(is_published=True)
    serializer_class = TopicSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def perform_update(self, serializer):
        if not self.request.user.is_superuser:
            serializer.save(is_published=False)
        else:
            serializer.save()
