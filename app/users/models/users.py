from django.contrib.auth.models import AbstractUser, Group
from django.db import models


class User(AbstractUser):
    username = models.CharField(
        'Ник', max_length=64, unique=True, null=True, blank=True
    )
    email = models.EmailField('Почта', unique=True, null=True, blank=True)
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = 'Пользователь'


# Adding properties to Group model
Group.add_to_class(
    'code', models.CharField('Code', max_length=32, null=True, unique=True)
)
