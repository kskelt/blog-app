from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _
from app.users.models.users import User


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    change_user_password_template = None
    fieldsets = (
        (None, {'fields': ('email', 'username')}),
        (_('Личная информация'),
         {'fields': ('first_name', 'last_name',)}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_superuser', 'groups', 'user_permissions',),
        }),
        (_('Important dates'), {'fields': ('last_login',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1',),
        }),
    )
    list_display = ('id', 'get_full_name', 'email',)
    list_display_links = ('id', 'get_full_name',)
    list_filter = ('is_superuser', 'is_active', 'groups')
    search_fields = ('first_name', 'last_name', 'id', 'email',)
    ordering = ('-id',)
    filter_horizontal = ('groups', 'user_permissions',)
    readonly_fields = ('last_login',)

    def get_full_name(self, obj):
        return obj.get_full_name()
