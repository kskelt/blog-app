from django.urls import path, include
from rest_framework.routers import DefaultRouter

from app.users.views.users import UserViewSet

router = DefaultRouter()

router.register(r'search', UserViewSet, 'users')

urlpatterns = router.urls

urlpatterns += [
    path('auth/', include('djoser.urls.jwt')),
]
