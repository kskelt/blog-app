from django.contrib.auth import get_user_model
from rest_framework import viewsets
from rest_framework.filters import SearchFilter
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser

from app.users.serializers.users import UserSerializer
from drf_spectacular.utils import extend_schema
from django.db.models import Q

User = get_user_model()

@extend_schema(tags=['Аутентификация & Авторизация'])
class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer  
    filter_backends = [SearchFilter]
    search_fields = ('last_name', 'email', 'username',)

    def get_queryset(self):
        user = self.request.user
        return User.objects.filter(id=user.id)

    def get_permissions(self):
        if self.action in ('create', 'update'):
            permissions = (AllowAny,)
        elif self.action in ('list',):
            permissions = (IsAdminUser,) 
        else:
          permissions = (IsAuthenticated,)
        return [permission() for permission in permissions]
    
    def perform_create(self, serializer):
        serializer.save()
        
    def get_serializer_class(self):
        if self.action == 'list':
            return UserSerializer  
        return UserSerializer
