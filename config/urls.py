
from django.contrib import admin
from django.urls import path, include
from drf_spectacular.views import SpectacularAPIView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('users/', include('app.users.urls')),
    path('post/', include('app.post.urls')),
    path('topic/', include('app.post.urls')),
    path('schema/', SpectacularAPIView.as_view(), name='schema'),
]
